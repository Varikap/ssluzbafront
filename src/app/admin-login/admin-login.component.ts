import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import { Router } from '@angular/router'
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css', '../matindigo.css']
})
export class AdminLoginComponent implements OnInit {

  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;

  getErrorMessageu() {
    return this.username.hasError('required') ? 'Unesi username' :
      this.username.hasError('username') ? 'Not a valid username' :
        '';
  }
  getErrorMessagep() {
    return this.password.hasError('required') ? 'Unesi ga password' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }
  token: string;
  login(username, password) {
    console.log("USERNAME JE >>> " + username.value);
    console.log("PASS >>>>>>>" + password.value);
    const role = "ADMIN";
    this.authService.login(username.value, password.value, role).subscribe(response => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response.token)
    },
      errorResponse => {
        this.showErrorMessage = true;
      }
    );
  }

  constructor(private authService: AuthServiceService,
    private router: Router) { }

  ngOnInit() {
    this.authService.logout();
  }

}
