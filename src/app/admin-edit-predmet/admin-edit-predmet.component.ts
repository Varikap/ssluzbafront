import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';
import { PredmetServiceService } from '../service/predmet-service.service';


@Component({
  selector: 'app-admin-edit-predmet',
  templateUrl: './admin-edit-predmet.component.html',
  styleUrls: ['./admin-edit-predmet.component.css', './matindigothome.css']
})
export class AdminEditPredmetComponent implements OnInit {

  name = new FormControl('', Validators.required);
  oznaka = new FormControl('', Validators.required);
  espb = new FormControl('', Validators.required);
  godina = new FormControl('', Validators.required);
  smer = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;

  dropdown = false;
  private admin: any;
  private smerovi: Array<any> = [];
  private predmeti: Array<any> = [];
  selectedPredmet: any;
  onSelect(data: any): void {
    this.selectedPredmet = data;
    console.log(this.selectedPredmet);
}


  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  editPredmet(name, oznaka, espb, godina, smer) {
    console.log(name.value, oznaka.value, espb.value, godina.value);
    var espbI = +espb.value;
    var godinaI = +godina.value;
    var smerId = +smer.value;
    console.log(godinaI);
    this.adminService.editPredmet(this.selectedPredmet.id, name.value, oznaka.value, espbI, godinaI, smerId).subscribe((response) => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }



  constructor(private predmetService: PredmetServiceService, private smerService: SmerServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.smerService.getSmerovi().subscribe(data => { this.smerovi = data; console.log(this.smerovi[0].naziv) });
    this.predmetService.getPredmeti().subscribe(data => { this.predmeti = data; console.log(this.predmeti[0].name) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.admin = data; console.log("DATA JE: " + data.name) });
  }

}
