import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditPredmetComponent } from './admin-edit-predmet.component';

describe('AdminEditPredmetComponent', () => {
  let component: AdminEditPredmetComponent;
  let fixture: ComponentFixture<AdminEditPredmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditPredmetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditPredmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
