import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NepolozeniPredmetiComponent } from './nepolozeni-predmeti.component';

describe('NepolozeniPredmetiComponent', () => {
  let component: NepolozeniPredmetiComponent;
  let fixture: ComponentFixture<NepolozeniPredmetiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NepolozeniPredmetiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NepolozeniPredmetiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
