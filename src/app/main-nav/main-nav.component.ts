import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, share } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css', '../matindigo.css']
})
export class MainNavComponent {

  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);

  getErrorMessageu() {
    return this.username.hasError('required') ? 'You must enter a value' :
      this.username.hasError('username') ? 'Not a valid username' :
        '';
  }
  getErrorMessagep() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }

  constructor(private breakpointObserver: BreakpointObserver) {}

}
