import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';
import { StudentServiceService } from '../service/student-service.service';
import { PredmetServiceService } from '../service/predmet-service.service';

@Component({
  selector: 'app-zakazi-ispit',
  templateUrl: './zakazi-ispit.component.html',
  styleUrls: ['./zakazi-ispit.component.css', './matindigothome.css']
})
export class ZakaziIspitComponent implements OnInit {

  rok = new FormControl('', Validators.required);
  date = new FormControl('', Validators.required);
  predmet = new FormControl('', Validators.required);

  dropdown = false;
  private teacher: any;
  private predmeti: Array<any> = [];
  private rokovi: Array<any> = [];
  selectedPredmet: any;
  showErrorMessage: boolean = false;
  onSelect(data: any): void {
    this.selectedPredmet = data;
    console.log(this.selectedPredmet);
}
  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  zakaziIspit(date, rok) {
    var rokId = +rok.value;
    console.log(date.value);
    console.log(rokId);
    this.predmetService.makeExam(this.selectedPredmet.id, date.value, rokId).subscribe((response) => {
      this.router.navigate(['/teacher-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }

  constructor(private predmetService: PredmetServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.predmetService.getRokovi().subscribe(data => { this.rokovi = data; console.log(this.rokovi[0].naziv) });
    this.predmetService.getPredmetiPredavaca().subscribe(data => { this.predmeti = data; console.log(this.predmeti[0].name) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.teacher = data; console.log("DATA JE: " + data.name) });
  }

}
