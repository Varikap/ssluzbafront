import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZakaziIspitComponent } from './zakazi-ispit.component';

describe('ZakaziIspitComponent', () => {
  let component: ZakaziIspitComponent;
  let fixture: ComponentFixture<ZakaziIspitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZakaziIspitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZakaziIspitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
