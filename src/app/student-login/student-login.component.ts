import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../service/auth-service.service';
import { Router } from '@angular/router'


@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css', '../matindigo.css']
})
export class StudentLoginComponent implements OnInit {
  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;

  getErrorMessageu() {
    return this.username.hasError('required') ? 'You must enter a value' :
      this.username.hasError('username') ? 'Not a valid username' :
        '';
  }
  getErrorMessagep() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }
  token: string;
  login(username, password) {
    console.log("USERNAME JE >>> " + username.value);
    console.log("PASS >>>>>>>" + password.value);
    var role = "STUDENT";
    console.log(role);
    this.authService.login(username.value, password.value, role).subscribe(response => {
      this.router.navigate(['/student-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response.token)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );
  }

  constructor(private authService: AuthServiceService,
    private router: Router) { }

  ngOnInit() {
    this.authService.logout();
  }

}
