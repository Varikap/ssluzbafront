import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';

@Component({
  selector: 'app-edit-smer',
  templateUrl: './edit-smer.component.html',
  styleUrls: ['./edit-smer.component.css', './matindigothome.css']
})
export class EditSmerComponent implements OnInit {

  naziv = new FormControl('', Validators.required);
  nivo = new FormControl('', Validators.required);
  espb = new FormControl('', Validators.required);
  

  dropdown = false;
  private admin: any;
  private nivoi: Array<any> = [];
  private smerovi: Array<any> = [];
  showErrorMessage: boolean = false;

  selectedSmer: any;
  onSelect(data: any): void {
    this.selectedSmer = data;
    console.log(this.selectedSmer);
}


  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  editSmer(naziv, nivo, espb) {
    console.log(naziv.value, nivo.value, espb.value);
    var espbI = +espb.value;
    this.smerService.editSmer(this.selectedSmer.id, naziv.value, nivo.value, espbI).subscribe((response) => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }



  constructor(private smerService: SmerServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.smerService.getNivoe().subscribe(data => { this.nivoi = data; console.log(this.nivoi[0].name) });
    this.smerService.getSmerovi().subscribe(data => { this.smerovi = data; console.log(this.smerovi[0].name) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.admin = data; console.log("DATA JE: " + data.name) });
  }

}
