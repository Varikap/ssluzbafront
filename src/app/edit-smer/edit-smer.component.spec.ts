import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSmerComponent } from './edit-smer.component';

describe('EditSmerComponent', () => {
  let component: EditSmerComponent;
  let fixture: ComponentFixture<EditSmerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSmerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
