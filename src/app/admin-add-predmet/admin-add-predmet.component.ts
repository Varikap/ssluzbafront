import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';

@Component({
  selector: 'app-admin-add-predmet',
  templateUrl: './admin-add-predmet.component.html',
  styleUrls: ['./admin-add-predmet.component.css', './matindigothome.css']
})
export class AdminAddPredmetComponent implements OnInit {

  name = new FormControl('', Validators.required);
  oznaka = new FormControl('', Validators.required);
  espb = new FormControl('', Validators.required);
  godina = new FormControl('', Validators.required);
  smer = new FormControl('', Validators.required);

  dropdown = false;
  private admin: any;
  private smerovi: Array<any> = [];
  showErrorMessage: boolean = false;
  selectedSmer: any;
  selectSmer(data: any): void {
    this.selectedSmer = data.id;
    console.log(this.selectedSmer);
}


  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  addPredmet(name, oznaka, espb, godina, smer) {
    console.log(name.value, oznaka.value, espb.value, godina.value);
    var espbI = +espb.value;
    var godinaI = +godina.value;
    var smerId = +smer.value;
    console.log(godinaI);
    this.adminService.addPredmet(name.value, oznaka.value, espbI, godinaI, smerId).subscribe((response) => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }



  constructor(private smerService: SmerServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.smerService.getSmerovi().subscribe(data => { this.smerovi = data; console.log(this.smerovi[0].naziv) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.admin = data; console.log("DATA JE: " + data.name) });
  }

}
