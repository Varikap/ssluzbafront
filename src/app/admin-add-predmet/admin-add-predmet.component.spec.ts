import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddPredmetComponent } from './admin-add-predmet.component';

describe('AdminAddPredmetComponent', () => {
  let component: AdminAddPredmetComponent;
  let fixture: ComponentFixture<AdminAddPredmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAddPredmetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddPredmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
