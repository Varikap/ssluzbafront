import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OdjaviIspitComponent } from './odjavi-ispit.component';

describe('OdjaviIspitComponent', () => {
  let component: OdjaviIspitComponent;
  let fixture: ComponentFixture<OdjaviIspitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OdjaviIspitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OdjaviIspitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
