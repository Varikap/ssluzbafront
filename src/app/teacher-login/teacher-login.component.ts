import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import { Router } from '@angular/router'
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-teacher-login',
  templateUrl: './teacher-login.component.html',
  styleUrls: ['./teacher-login.component.css', '../matindigo.css']
})
export class TeacherLoginComponent implements OnInit {

  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;

  getErrorMessageu() {
    return this.username.hasError('required') ? 'Unesi ga coece' :
      this.username.hasError('username') ? 'Not a valid username' :
        '';
  }
  getErrorMessagep() {
    return this.password.hasError('required') ? 'Unesi ga coece' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }
  token: string;
  login(username, password) {
    console.log("USERNAME JE >>> " + username);
    console.log("PASS >>>>>>>" + password);
    const role = "PREDAVAC";
    this.authService.login(username.value, password.value, role).subscribe(response => {
      this.router.navigate(['/teacher-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response.token)
    },
      errorResponse => {
        this.showErrorMessage = true;
      }
    );
  }
  constructor(private authService: AuthServiceService,
              private router: Router) { }

  ngOnInit() {
    this.authService.logout();
  }

}
