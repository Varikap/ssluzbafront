import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-teacher-home',
  templateUrl: './teacher-home.component.html',
  styleUrls: ['./teacher-home.component.css', './matindigothome.css']
})
export class TeacherHomeComponent implements OnInit {
  dropdown = false;
  private teacher: any;

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }

  constructor(private studentService: StudentServiceService, private authService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/teacher-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.teacher = data; console.log("DATA JE: " + data.name) });
    console.log(this.teacher);
  }

}
