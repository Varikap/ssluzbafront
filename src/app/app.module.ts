import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSidenavModule, MatIconModule, MatListModule, MatButtonModule, MatFormFieldModule, MatSelectModule, MatInputModule } from '@angular/material';
import { MainNavComponent } from './main-nav/main-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatTableModule } from '@angular/material/table';
import { StudentLoginComponent } from './student-login/student-login.component';
import { TeacherLoginComponent } from './teacher-login/teacher-login.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule }    from '@angular/common/http';
import { TeacherHomeComponent } from './teacher-home/teacher-home.component';
import { StudentHomeComponent } from './student-home/student-home.component';
import { PrijaviIspitComponent } from './prijavi-ispit/prijavi-ispit.component';
import { PolozeniPredmetiComponent } from './polozeni-predmeti/polozeni-predmeti.component';
import { NepolozeniPredmetiComponent } from './nepolozeni-predmeti/nepolozeni-predmeti.component';
import { ZakaziIspitComponent } from './zakazi-ispit/zakazi-ispit.component';
import { OceniIspitComponent } from './oceni-ispit/oceni-ispit.component';
import { StudentEditStudentComponent } from './student-edit-student/student-edit-student.component';
import { TeacherEditTeacherComponent } from './teacher-edit-teacher/teacher-edit-teacher.component';
import { AdminEditStudentComponent } from './admin-edit-student/admin-edit-student.component';
import { AdminAddStudentComponent } from './admin-add-student/admin-add-student.component';
import { AdminAddTeacherComponent } from './admin-add-teacher/admin-add-teacher.component';
import { AdminAddPredmetComponent } from './admin-add-predmet/admin-add-predmet.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminAddSmerComponent } from './admin-add-smer/admin-add-smer.component';
import { AdminAddAdminComponent } from './admin-add-admin/admin-add-admin.component';
import { AdminEditPredmetComponent } from './admin-edit-predmet/admin-edit-predmet.component';
import { EditSmerComponent } from './edit-smer/edit-smer.component';
import { UplatiComponent } from './uplati/uplati.component';
import { OdjaviIspitComponent } from './odjavi-ispit/odjavi-ispit.component';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    StudentLoginComponent,
    TeacherLoginComponent,
    HomeComponent,
    TeacherHomeComponent,
    StudentHomeComponent,
    PrijaviIspitComponent,
    PolozeniPredmetiComponent,
    NepolozeniPredmetiComponent,
    ZakaziIspitComponent,
    OceniIspitComponent,
    StudentEditStudentComponent,
    TeacherEditTeacherComponent,
    AdminEditStudentComponent,
    AdminAddStudentComponent,
    AdminAddTeacherComponent,
    AdminAddPredmetComponent,
    AdminLoginComponent,
    AdminHomeComponent,
    AdminAddSmerComponent,
    AdminAddAdminComponent,
    AdminEditPredmetComponent,
    EditSmerComponent,
    UplatiComponent,
    OdjaviIspitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    LayoutModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatFormFieldModule,
    MatSelectModule,
    HttpClientModule,
    FormsModule,
    MatInputModule,
    MatTableModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
