import { Component, OnInit } from '@angular/core';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AuthServiceService } from '../service/auth-service.service';
import { StudentServiceService } from '../service/student-service.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-uplati',
  templateUrl: './uplati.component.html',
  styleUrls: ['./uplati.component.css', './matindigothome.css']
})
export class UplatiComponent implements OnInit {

  svrha = new FormControl('',Validators.required);
  iznos = new FormControl('',Validators.required);
  dropdown = false;
  private student: any;
  showErrorMessage: boolean = false;

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }

  Uplati(svrha, iznos){
    var iznosNum = +iznos.value;
    console.log(svrha.value, iznos.value);
    this.studentService.uplatiNa(iznosNum, svrha.value).subscribe((response) => {
      this.router.navigate(['/student-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }

  constructor(private studentService: StudentServiceService, private authService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/student-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.student = data; console.log("DATA JE: " + data.name) });
    console.log(this.student);
  }

}
