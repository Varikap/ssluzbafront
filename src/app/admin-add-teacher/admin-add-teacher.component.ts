import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-add-teacher',
  templateUrl: './admin-add-teacher.component.html',
  styleUrls: ['./admin-add-teacher.component.css', './matindigothome.css']
})
export class AdminAddTeacherComponent implements OnInit {

  name = new FormControl('', Validators.required);
  surname = new FormControl('', Validators.required);
  jmbg = new FormControl('', Validators.required);
  email = new FormControl('', Validators.required);
  username = new FormControl('', Validators.required);
  password = new FormControl('', Validators.required);
  zvanje = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;

  dropdown = false;
  private admin: any;
  private zvanja: Array<any> = [];

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  addPredavac(name, surname, jmbg, email, username, password, zvanje) {
    console.log(name.value, surname.value, jmbg.value, email.value, username.value, password.value);
    this.adminService.addPredavac(name.value, surname.value, jmbg.value, email.value, username.value, password.value, zvanje.value).subscribe((response) => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }

  constructor(private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.adminService.getZvanja().subscribe(data => { this.zvanja = data; console.log(this.zvanja[0].name) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.admin = data; console.log("DATA JE: " + data.name) });
  }
  

}
