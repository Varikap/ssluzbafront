import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthServiceService } from '../service/auth-service.service';
import { Router } from '@angular/router'
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-student-edit-student',
  templateUrl: './student-edit-student.component.html',
  styleUrls: ['./student-edit-student.component.css', './matindigothome.css']
})
export class StudentEditStudentComponent implements OnInit {

  password = new FormControl('', Validators.required);
  showErrorMessage: boolean = false;
  dropdown = false;
  private student: any;

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }

  getErrorMessagep() {
    return this.password.hasError('required') ? 'You must enter a value' :
      this.password.hasError('password') ? 'Not a valid username' :
        '';
  }
  token: string;
  change(password) {
    console.log("PASS >>>>>>>" + password.value);
    var role = "STUDENT";
    console.log(role);
    this.studentService.ChangePassword(password.value, role).subscribe(response => {
      this.router.navigate(['/student-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );
  }

  constructor(private studentService: StudentServiceService, private authService: AuthServiceService,
    private router: Router) { }

  ngOnInit() {
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/student-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.student = data; console.log("DATA JE: " + data.name) });
    console.log(this.student);
  }

}
