import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentEditStudentComponent } from './student-edit-student.component';

describe('StudentEditStudentComponent', () => {
  let component: StudentEditStudentComponent;
  let fixture: ComponentFixture<StudentEditStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentEditStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentEditStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
