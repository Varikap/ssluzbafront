import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherEditTeacherComponent } from './teacher-edit-teacher.component';

describe('TeacherEditTeacherComponent', () => {
  let component: TeacherEditTeacherComponent;
  let fixture: ComponentFixture<TeacherEditTeacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherEditTeacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherEditTeacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
