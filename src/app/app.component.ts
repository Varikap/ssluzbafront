import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css', './matindigo.css']
})
export class AppComponent {
  title = 'SSservis';
}
