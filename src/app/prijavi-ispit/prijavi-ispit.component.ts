import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../service/auth-service.service';
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-prijavi-ispit',
  templateUrl: './prijavi-ispit.component.html',
  styleUrls: ['./prijavi-ispit.component.css', './matindigothome.css']
})
export class PrijaviIspitComponent implements OnInit {
  dropdown = false;
  private ispiti: Array<any> = [];
  private ispitiIds: Array<any> = [];
  showErrorMessage: boolean = false;
  private student: any;

  selectedIspit: any;
  dodajIspit(data: any, checkboxId){
    this.selectedIspit = data;
    var checkBoxI = document.getElementById(checkboxId) as HTMLInputElement;
    if (checkBoxI.checked == true){
      var theid = +data.id
      this.ispitiIds.push(theid);
      console.log(this.ispitiIds);
    } else {
      var theid = +data.id
      this.ispitiIds.forEach((isp, index)=>{
        if(theid == isp){
          this.ispitiIds.splice(index, 1);
          console.log(this.ispitiIds);
        }
      });
      console.log(this.ispitiIds);
    }
  }
  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  PrijaviIspit(){
    this.studentService.PrijaviIspit(this.ispitiIds).subscribe((response) => {
      this.router.navigate(['/student-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );
  }
  constructor(private studentService: StudentServiceService, private authService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    this.studentService.getIspiti().subscribe(data => { this.ispiti = data; console.log("eeeeeeeeeeeeeee"+this.ispiti[0].id) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/student-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.student = data; console.log("DATA JE: " + data.name) });
    console.log(this.student);
  }

}
