import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrijaviIspitComponent } from './prijavi-ispit.component';

describe('PrijaviIspitComponent', () => {
  let component: PrijaviIspitComponent;
  let fixture: ComponentFixture<PrijaviIspitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrijaviIspitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrijaviIspitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
