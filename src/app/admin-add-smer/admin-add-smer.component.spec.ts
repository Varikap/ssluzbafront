import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddSmerComponent } from './admin-add-smer.component';

describe('AdminAddSmerComponent', () => {
  let component: AdminAddSmerComponent;
  let fixture: ComponentFixture<AdminAddSmerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAddSmerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddSmerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
