import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';
import { RequestOptions } from '@angular/http';
import {baseUrl} from '../utils/Const';

@Injectable({
  providedIn: 'root'
})
export class StudentServiceService {

  constructor(private http: HttpClient) { }

  getLoggedIn(): any {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };
    return this.http.get(`${baseUrl}/api/ulogovan`, httpOptions);
  }

  addStudent(name: string, surname: string, indexBr: string, jmbg: string, smerId: number, godina: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${baseUrl}/api/create/student`, { name, surname, indexBr, jmbg, smerId, godina }, httpOptions);
  }
  uplatiNa(iznos: number, svrhaUplate: string) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.put(`${baseUrl}/api/studenti/uplata`, { iznos, svrhaUplate }, httpOptions);
  }
  PrijaviIspit(ispitIDs: Array<any> = []) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.put(`${baseUrl}/api/studenti/prijavi`, { ispitIDs }, httpOptions);
  }
  OdjaviIspit(ispitIDs: Array<any> = []) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.put(`${baseUrl}/api/studenti/odjavi`, { ispitIDs }, httpOptions);
  }
  editStudent(id: number, name: string, surname: string, indexBr: string, jmbg: string, smerId: number, godina: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.put(`${baseUrl}/api/studenti/${id}`, {id, name, surname, indexBr, jmbg, smerId, godina }, httpOptions);
  }
  ChangePassword(novaSifra: string, role: string) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.put(`${baseUrl}/api/nova`, { novaSifra }, httpOptions);
  }
  getStudenti(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(`${baseUrl}/api/studenti/all`, httpOptions);
  }
  getIspiti(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/studenti/moguci`, httpOptions);
  }
  getNepolozeni(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/studenti/nepolozeni`, httpOptions);
  }
  getPolozeni(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/studenti/polozeni`, httpOptions);
  }
  getIspitiZaOdjavu(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/studenti/prijavljeni`, httpOptions);
  }



}
