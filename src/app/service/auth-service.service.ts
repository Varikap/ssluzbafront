import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';
import {baseUrl} from '../utils/Const';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  constructor(private http: HttpClient) { }

  login(username : string, password : string, role : string){
      const httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
      };
    return this.http.post<Token>(`${baseUrl}/api/login`, {username, password, role}, httpOptions).pipe(tap(res => this.setSession(res.token)));
  }

  private setSession(token: string) {
    console.log("POZVANA SAM");
    console.log(token);
    sessionStorage.setItem("token", token);
  }

  private log(res:string) {
    console.log(res);
    return res;
  }
  logout() {
    sessionStorage.removeItem('token');
  }
  jeste = true;
  isUserLoggedIn() {
    let user = sessionStorage.getItem('token');
    console.log(user);
    if (user === null){
      this.jeste = false;
    }
    return this.jeste;
  }

}
