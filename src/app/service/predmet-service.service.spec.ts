import { TestBed } from '@angular/core/testing';

import { PredmetServiceService } from './predmet-service.service';

describe('PredmetServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PredmetServiceService = TestBed.get(PredmetServiceService);
    expect(service).toBeTruthy();
  });
});
