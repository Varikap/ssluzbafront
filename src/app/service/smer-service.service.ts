import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';
import { RequestOptions } from '@angular/http';
import {baseUrl} from '../utils/Const';

@Injectable({
  providedIn: 'root'
})
export class SmerServiceService {

  constructor(private http: HttpClient) { }
  addSmer(naziv: string, nivo: string, espb: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${baseUrl}/api/smerovi`, { naziv, nivo, espb}, httpOptions);
  }
  editSmer(id: number, naziv: string, nivo: string, espb: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.put(`${baseUrl}/api/smerovi/${id}`, {id, naziv, nivo, espb}, httpOptions);
  }

  getNivoe(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(`${baseUrl}/api/smerovi/nivoi`, httpOptions);
  }
  getSmerovi(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(`${baseUrl}/api/smerovi/all`, httpOptions);
  }
}
