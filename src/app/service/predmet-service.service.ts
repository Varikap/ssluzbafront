import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';
import { RequestOptions } from '@angular/http';
import {baseUrl} from '../utils/Const';

@Injectable({
  providedIn: 'root'
})
export class PredmetServiceService {

  constructor(private http: HttpClient) { }


  getPredmeti(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(`${baseUrl}/api/predmeti/all`, httpOptions);
  }
  getIspitiZaOcenu(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/predavaci/zakazani`, httpOptions);
  }
  getPredmetiPredavaca(): Observable<any> {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/predavaci/predmeti`, httpOptions);
  }
  getRokovi(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.get(`${baseUrl}/api/rok/all`, httpOptions);
  }
  makeExam(predmetID: number, datum_polaganja: string, rokID: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${baseUrl}/api/ispiti`, { predmetID, datum_polaganja, rokID }, httpOptions);
  }
  oceniIspit(id: number, ocena: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.put(`${baseUrl}/api/predavaci/oceni/${id}?ocena=${ocena}`,{id, ocena}, httpOptions);
  }
}
