import { TestBed } from '@angular/core/testing';

import { SmerServiceService } from './smer-service.service';

describe('SmerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SmerServiceService = TestBed.get(SmerServiceService);
    expect(service).toBeTruthy();
  });
});
