import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { Token } from '../model/Token';
import { RequestOptions } from '@angular/http';
import {baseUrl} from '../utils/Const';

@Injectable({
  providedIn: 'root'
})
export class AdminServiceService {

  constructor(private http: HttpClient) { }

  addAdmin(name : string, surname : string, jmbg : string, email : string, username : string, password : string){
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${baseUrl}/api/create/admin`, {name, surname, jmbg, email, username, password}, httpOptions);
  }

  addPredavac(name: string, surname: string, jmbg: string, email: string, username: string, password: string, zvanje: string) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.post(`${baseUrl}/api/create/predavac`, { name, surname, jmbg, email, username, password, zvanje}, httpOptions);
  }

  addPredmet(name: string, oznaka: string, espb: number, godina: number, smerId: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.post(`${baseUrl}/api/predmeti`, { name, oznaka, espb, godina, smerId }, httpOptions);
  }
  editPredmet(id: number, name: string, oznaka: string, espb: number, godina: number, smerId: number) {
    let token = sessionStorage.getItem('token');
    console.log(token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    return this.http.put(`${baseUrl}/api/predmeti/${id}`, {id, name, oznaka, espb, godina, smerId }, httpOptions);
  }

  getZvanja(): Observable<any> {
    let token = sessionStorage.getItem('token');
    console.log("EEEEEEEEEEEEEEEEEEVO ga"+token);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };
    return this.http.get(`${baseUrl}/api/predavaci/zvanje`, httpOptions);
  }


  getLoggedIn(): any {
    let token = sessionStorage.getItem('token');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'X-Auth-Token': token
      })
    };

    return this.http.get(`${baseUrl}/api/ulogovan/`, httpOptions);
  }
}
