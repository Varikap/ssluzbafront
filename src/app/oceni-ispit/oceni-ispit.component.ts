import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';
import { StudentServiceService } from '../service/student-service.service';
import { PredmetServiceService } from '../service/predmet-service.service';

@Component({
  selector: 'app-oceni-ispit',
  templateUrl: './oceni-ispit.component.html',
  styleUrls: ['./oceni-ispit.component.css', './matindigothome.css']
})
export class OceniIspitComponent implements OnInit {

  predmet = new FormControl('', Validators.required);
  ocena = new FormControl('', Validators.required);

  dropdown = false;
  private teacher: any;
  private ispiti: Array<any> = [];
  selectedIspit: any;
  showErrorMessage: boolean = false;
  onSelect(data: any): void {
    this.selectedIspit = data;
    console.log(this.selectedIspit);
}
  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  oceniIspit(ocena) {
    var ocenaI = +ocena.value;
    console.log(ocena.value);
    console.log(ocenaI);
    this.predmetService.oceniIspit(this.selectedIspit.id, ocenaI).subscribe((response) => {
      this.router.navigate(['/teacher-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }

  constructor(private predmetService: PredmetServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.predmetService.getIspitiZaOcenu().subscribe(data => { this.ispiti = data; });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.teacher = data; console.log("DATA JE: " + data.name) });
  }

}
