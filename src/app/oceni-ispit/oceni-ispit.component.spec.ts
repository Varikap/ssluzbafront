import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OceniIspitComponent } from './oceni-ispit.component';

describe('OceniIspitComponent', () => {
  let component: OceniIspitComponent;
  let fixture: ComponentFixture<OceniIspitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OceniIspitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OceniIspitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
