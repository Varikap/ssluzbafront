import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentLoginComponent } from './student-login/student-login.component';
import { TeacherLoginComponent } from './teacher-login/teacher-login.component';
import { TeacherHomeComponent } from './teacher-home/teacher-home.component';
import { StudentHomeComponent } from './student-home/student-home.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminEditPredmetComponent } from './admin-edit-predmet/admin-edit-predmet.component';
import { AdminAddAdminComponent } from './admin-add-admin/admin-add-admin.component';
import { AdminAddStudentComponent } from './admin-add-student/admin-add-student.component';
import { AdminAddTeacherComponent } from './admin-add-teacher/admin-add-teacher.component';
import { AdminAddPredmetComponent } from './admin-add-predmet/admin-add-predmet.component';
import { PolozeniPredmetiComponent } from './polozeni-predmeti/polozeni-predmeti.component';
import { AdminAddSmerComponent } from './admin-add-smer/admin-add-smer.component';
import { EditSmerComponent } from './edit-smer/edit-smer.component';
import { AdminEditStudentComponent } from './admin-edit-student/admin-edit-student.component';
import { ZakaziIspitComponent } from './zakazi-ispit/zakazi-ispit.component';
import { UplatiComponent } from './uplati/uplati.component';
import { PrijaviIspitComponent } from './prijavi-ispit/prijavi-ispit.component';
import { OdjaviIspitComponent } from './odjavi-ispit/odjavi-ispit.component';
import { OceniIspitComponent } from './oceni-ispit/oceni-ispit.component';
import { NepolozeniPredmetiComponent } from './nepolozeni-predmeti/nepolozeni-predmeti.component';
import { StudentEditStudentComponent } from './student-edit-student/student-edit-student.component';


const routes: Routes = [
  { path: 'student-login', component: StudentLoginComponent},
  { path: 'teacher-login', component: TeacherLoginComponent},
  { path: 'teacher-home', component: TeacherHomeComponent },
  { path: 'student-home', component: StudentHomeComponent },
  { path: 'admin-login', component: AdminLoginComponent},
  { path: 'admin-home', component: AdminHomeComponent },
  { path: 'edit-student', component: AdminEditStudentComponent },
  { path: 'edit-predmet', component: AdminEditPredmetComponent },
  { path: 'edit-smer', component: EditSmerComponent },
  { path: 'add-admin', component: AdminAddAdminComponent },
  { path: 'add-student', component: AdminAddStudentComponent },
  { path: 'add-teacher', component: AdminAddTeacherComponent },
  { path: 'add-class', component: AdminAddPredmetComponent },
  { path: 'add-smer', component: AdminAddSmerComponent },
  { path: 'polozeni-predmeti', component: PolozeniPredmetiComponent },
  { path: 'nepolozeni-predmeti', component: NepolozeniPredmetiComponent },
  { path: 'make-exam', component: ZakaziIspitComponent },
  { path: 'oceni', component: OceniIspitComponent },
  { path: 'uplati', component: UplatiComponent },
  { path: 'prijavi', component: PrijaviIspitComponent },
  { path: 'change-password', component: StudentEditStudentComponent },
  { path: 'odjavi', component: OdjaviIspitComponent },
  { path: '', component: TeacherLoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
