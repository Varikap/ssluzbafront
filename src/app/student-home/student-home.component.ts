import { Component, OnInit } from '@angular/core';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AuthServiceService } from '../service/auth-service.service';
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-student-home',
  templateUrl: './student-home.component.html',
  styleUrls: ['./student-home.component.css', './matindigothome.css']
})
export class StudentHomeComponent implements OnInit {

  dropdown = false;
  private student: any;

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  constructor(private studentService: StudentServiceService, private authService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/student-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.student = data; console.log("DATA JE: " + data.name) });
    console.log(this.student);
  }

}
