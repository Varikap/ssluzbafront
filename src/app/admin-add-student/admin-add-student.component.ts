import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../service/auth-service.service';
import * as angular from 'angular';
import { Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { SmerServiceService } from '../service/smer-service.service';
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-admin-add-student',
  templateUrl: './admin-add-student.component.html',
  styleUrls: ['./admin-add-student.component.css', './matindigothome.css']
})
export class AdminAddStudentComponent implements OnInit {

  name = new FormControl('', Validators.required);
  surname = new FormControl('', Validators.required);
  indexBr = new FormControl('', Validators.required);
  jbmg = new FormControl('', Validators.required);
  smer = new FormControl('', Validators.required);
  godina = new FormControl('', Validators.required);
  
  dropdown = false;
  private admin: any;
  private smerovi: Array<any> = [];
  private godine: Array<any> = [];
  showErrorMessage: boolean = false;
  selectedSmerGodina: number;
  addGodine(smeriD): void {
    this.godine=[];
    console.log(smeriD);
    var smerId = smeriD;
    console.log(smerId);
    for (let data of this.smerovi){
      if(data.id==smerId){
        this.selectedSmerGodina = data.trajanje;
      }
    }
    for(var i = 1; i<=this.selectedSmerGodina; i++){
      this.godine.push(i);
    }
    console.log(this.godine);
}

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  addStudent(name, surname, indexBr, jbmg, smer, godina) {
    var smerId = +smer.value;
    console.log(name.value, surname.value, godina.value);
    var godinaI = +godina.value;
    this.studentService.addStudent(name.value, surname.value, indexBr.value, jbmg.value, smerId, godinaI).subscribe((response) => {
      this.router.navigate(['/admin-home']); console.log("TOKEN JE >>>>>>>>>>>>>" + response)
        },
  errorResponse => {
    this.showErrorMessage = true;
  }
    );

  }



  constructor(private studentService: StudentServiceService, private smerService: SmerServiceService, private http: HttpClient, private adminService: AdminServiceService, private authService: AuthServiceService, private router: Router, private fb: FormBuilder) {
  }

  ngOnInit() {
    this.smerService.getSmerovi().subscribe(data => { this.smerovi = data; console.log(this.smerovi[0].name) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/admin-login']);
    }
    this.adminService.getLoggedIn().subscribe(data => { this.admin = data; console.log("DATA JE: " + data.name) });
  }

}
