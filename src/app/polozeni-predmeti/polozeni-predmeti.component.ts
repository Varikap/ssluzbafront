import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthServiceService } from '../service/auth-service.service';
import { StudentServiceService } from '../service/student-service.service';

@Component({
  selector: 'app-polozeni-predmeti',
  templateUrl: './polozeni-predmeti.component.html',
  styleUrls: ['./polozeni-predmeti.component.css', './matindigothome.css']
})
export class PolozeniPredmetiComponent implements OnInit {

  dropdown = false;
  private ispiti: Array<any> = [];
  private ispitiIds: Array<any> = [];
  showErrorMessage: boolean = false;
  private student: any;

  navigationDrop() {
    this.dropdown = true;
  }
  navigationUp() {
    this.dropdown = false;
  }
  logout() {
    this.authService.logout();
  }
  constructor(private studentService: StudentServiceService, private authService: AuthServiceService, private router: Router) { }

  ngOnInit() {
    this.studentService.getPolozeni().subscribe(data => { this.ispiti = data; console.log("eeeeeeeeeeeeeee"+this.ispiti[0].id) });
    let ulogovan = this.authService.isUserLoggedIn();
    console.log(ulogovan);
    if (ulogovan == false) {
      this.router.navigate(['/student-login']);
    }
    this.studentService.getLoggedIn().subscribe(data => { this.student = data; console.log("DATA JE: " + data.name) });
    console.log(this.student);
  }

}
