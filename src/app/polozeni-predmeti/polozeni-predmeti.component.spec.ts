import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolozeniPredmetiComponent } from './polozeni-predmeti.component';

describe('PolozeniPredmetiComponent', () => {
  let component: PolozeniPredmetiComponent;
  let fixture: ComponentFixture<PolozeniPredmetiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolozeniPredmetiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolozeniPredmetiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
